
import { movieApi } from '../services/movieApi'
import { bookApi } from '../services/bookApi'
import rootReducer from './root'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import { persistReducer, persistStore } from 'redux-persist'

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = configureStore({
  devTools: __DEV__,
  middleware: getDefaultMiddleware({
    serializableCheck: false,
  }).concat([bookApi.middleware]), // NOTE this addition
  reducer: persistedReducer,
})

export const persistor = persistStore(store)
setupListeners(store.dispatch) // NOTE this addition

export default store