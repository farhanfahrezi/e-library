import { configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import { movieApi } from '../services/movieApi'
import { bookApi } from '../services/bookApi'

export const store = configureStore({
  reducer: {
    // [movieApi.reducerPath]: movieApi.reducer,
    [bookApi.reducerPath]: bookApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(bookApi.middleware),
})

setupListeners(store.dispatch)