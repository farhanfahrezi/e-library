
import { combineReducers } from '@reduxjs/toolkit';
import { movieApi } from '../services/movieApi'
import { bookApi } from '../services/bookApi'

export default combineReducers({
  [movieApi.reducerPath]: movieApi.reducer,
  [bookApi.reducerPath]: bookApi.reducer,
});