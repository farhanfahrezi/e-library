import {BlurView} from '@react-native-community/blur';
import React, {useEffect, useState} from 'react';
import {
  Animated,
  Dimensions,
  ImageBackground,
  StyleSheet,
  useColorScheme,
  View,
} from 'react-native';
import Carousel from 'react-native-reanimated-carousel';
import {SafeAreaView} from 'react-native-safe-area-context';

import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useGetHarryPotterQuery} from '../../services/bookApi';
import {BookType} from '../../types/BookType';
import BigPoster from '../BigPoster';
import FFText from '../FFText';

const GradientImg = require('../../assets/images/bg-gradient.png');
const GradientWhiteImg = require('../../assets/images/bg-gradient-white.png');

function BigPosterSection({navigation}: any): JSX.Element {
  const [scrollY] = useState(new Animated.Value(0));

  const isDarkMode = useColorScheme() === 'dark';
  const windowWidth = Dimensions.get('window').width;
  const {
    data = {
      docs: [1, 2, 3],
    },
    currentData,
    error,
    isLoading,
    isFetching,
    refetch,
  } = useGetHarryPotterQuery('all');

  useEffect(() => {
    refetch();
  }, []);

  const goToDetail = (item: BookType) => {
    ReactNativeHapticFeedback.trigger('impactLight');
    navigation.navigate(
      'BookDetail',
      {book: item},
      {
        animationEnabled: false,
      },
    );
  };

  return (
    <View style={styles.container}>
      {data?.docs?.map((item: BookType, index: number) => (
        <Animated.Image
          key={`${item?.cover_i}-blur-cover`}
          source={{
            uri: data
              ? `https://covers.openlibrary.org/b/id/${
                  data?.docs[index]?.cover_id || data?.docs[index]?.cover_i
                }-L.jpg`
              : '',
          }}
          style={[
            styles.absolute,
            {
              opacity: scrollY.interpolate({
                inputRange: [index - 1, index, index + 1],
                outputRange: [0, 1, 0],
                extrapolate: 'clamp',
              }),
            },
          ]}
        />
      ))}

      <BlurView
        style={styles.absolute}
        blurType={isDarkMode ? 'dark' : 'light'}
        blurAmount={32}
      />

      {data && (
        <>
          <SafeAreaView edges={['top']} />
          <Carousel
            loop
            width={windowWidth}
            height={windowWidth * 1.4}
            autoPlay={false}
            autoFillData={true}
            data={data.docs || [1, 2, 3]}
            scrollAnimationDuration={300}
            windowSize={5}
            mode="parallax"
            modeConfig={{
              parallaxScrollingOffset: 70,
              parallaxAdjacentItemScale: 0.73,
              parallaxScrollingScale: 0.85,
            }}
            panGestureHandlerProps={{
              activeOffsetX: [-5, 5],
            }}
            style={{zIndex: 999}}
            onProgressChange={(
              offsetProgress: number,
              absoluteProgress: number,
            ) => {
              scrollY.setValue(absoluteProgress || 0);
              absoluteProgress % 1 === 0 &&
                ReactNativeHapticFeedback.trigger('impactLight');
            }}
            // onSnapToItem={(index: number) => {
            //   ReactNativeHapticFeedback.trigger('impactLight');
            // }}
            renderItem={({item}: any) => (
              <TouchableWithoutFeedback
                onPress={() => {
                  goToDetail(item);
                }}
                style={{width: '100%', height: '100%'}}>
                <BigPoster
                  onPress={() => {
                    goToDetail(item);
                  }}
                  imageUri={`https://covers.openlibrary.org/b/id/${
                    item?.cover_id || item?.cover_i
                  }-L.jpg`}></BigPoster>
              </TouchableWithoutFeedback>
            )}
          />
        </>
      )}

      {data?.docs?.map((item: BookType, index: number) => (
        <Animated.View
          key={`${item?.cover_i}-info`}
          style={[
            styles.infoContainer,
            {
              opacity: scrollY.interpolate({
                inputRange: [index - 0.5, index, index + 0.5],
                outputRange: [0, 1, 0],
                extrapolate: 'clamp',
              }),
              transform: [
                {
                  scale: scrollY.interpolate({
                    inputRange: [index - 1, index, index + 1],
                    outputRange: [0.8, 1, 0.8],
                    extrapolate: 'clamp',
                  }),
                },
              ],
            },
          ]}>
          <View style={styles.infoWrapper}>
            <FFText style={styles.title} numberOfLines={3}>
              {item.title || item.name}
            </FFText>
            <FFText style={styles.author}>
              {item?.first_publish_year} •{' '}
              {item?.author_name && item?.author_name[0]}
            </FFText>
          </View>
        </Animated.View>
      ))}

      <ImageBackground
        source={isDarkMode ? GradientImg : GradientWhiteImg}
        resizeMode="stretch"
        style={styles.gradientBg}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 310,
    paddingBottom: 100,
    marginTop: -220,
  },
  absolute: {
    ...StyleSheet.absoluteFillObject,
  },
  infoContainer: {
    paddingHorizontal: '7.5%',
    zIndex: 999999999,
  },
  infoWrapper: {
    position: 'absolute',
    top: -20,
    left: 0,
    right: 0,
    paddingHorizontal: '7.5%',
    zIndex: 999999999,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '700',
    letterSpacing: 0.5,
  },
  author: {
    marginTop: 6,
    textAlign: 'center',
    fontSize: 18,
    opacity: 0.7,
  },
  gradientBg: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 100,
    zIndex: 1,
  },
});

export default BigPosterSection;
