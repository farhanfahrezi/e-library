import {BlurView} from '@react-native-community/blur';
import React from 'react';
import {Image, StyleSheet, useColorScheme, View} from 'react-native';
import Theme from '../../utils/Theme';
import FFText from '../FFText';
import TouchableScale from '../TouchableScale';

const CloseImg = require('../../assets/images/close.png');
const CloseWhiteImg = require('../../assets/images/close-white.png');

type DetailPageHeaderProps = {
  title?: string;
  navigation?: any;
};

const DetailPageHeader = (props: DetailPageHeaderProps): JSX.Element => {
  const {navigation, title} = props;

  const isDarkMode = useColorScheme() === 'dark';

  return (
    <View style={styles.container}>
      <View style={[styles.absolute]}>
        <BlurView
          reducedTransparencyFallbackColor={Theme.Colors.dark}
          blurType={isDarkMode ? 'dark' : 'light'}
          overlayColor={
            isDarkMode ? 'rgba(0,0,0,0.5)' : 'rgba(255,255,255,0.5)'
          }
          blurAmount={32}
          style={styles.absolute}
        />
      </View>

      <View style={[styles.contentWrapper]}>
        <View style={[styles.content]}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              // alignItems: 'center',
            }}>
            <FFText style={styles.title}>{title}</FFText>
            <View>
              <TouchableScale
                onPress={() => navigation.pop()}
                style={styles.closeBtn}
                hitSlop={30}>
                <Image source={isDarkMode ? CloseWhiteImg : CloseImg} />
              </TouchableScale>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  absolute: {
    ...StyleSheet.absoluteFillObject,
  },
  closeBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 16,
    width: 30,
    height: 30,
  },
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    backgroundColor: 'transparent',
    overflow: 'hidden',
    zIndex: 99,
  },
  contentWrapper: {
    zIndex: 99,
  },
  content: {
    position: 'relative',
    marginVertical: 16,
  },
  title: {
    flex: 1,
    paddingLeft: 16,
    fontSize: 26,
    fontWeight: '500',
  },
  catWrapper: {
    flexDirection: 'row',
  },
  catButton: {
    marginRight: 16,
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderWidth: 1,
    borderRadius: 30,
    opacity: 1,
  },
  catName: {
    fontSize: 14,
    fontWeight: '700',
  },

  activeFilterWrapper: {
    paddingRight: 16,
    zIndex: 99,
  },
  catButtonActive: {
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderWidth: 1,
    borderRadius: 30,
    opacity: 1,
    zIndex: 999,
  },

  gradientBg: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    width: 56,
    borderBottomRightRadius: 14,
    borderBottomLeftRadius: 14,
    transform: [{rotate: '90deg'}, {translateY: -45}],
    zIndex: 1,
    opacity: 0.4,
    overflow: 'hidden',
  },
});

export default DetailPageHeader;
