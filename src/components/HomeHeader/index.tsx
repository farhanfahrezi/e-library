import {BlurView} from '@react-native-community/blur';
import React, {useState} from 'react';
import {
  ImageBackground,
  LayoutAnimation,
  Platform,
  StyleSheet,
  UIManager,
  useColorScheme,
  useWindowDimensions,
  View,
} from 'react-native';
import Animated, {FadeInRight, useAnimatedStyle} from 'react-native-reanimated';
import {SafeAreaView} from 'react-native-safe-area-context';
import {SUBJECTS} from '../../utils/Constant';
import Theme from '../../utils/Theme';
import FFText from '../FFText';
import TouchableScale from '../TouchableScale';

const GradientImg = require('../../assets/images/bg-gradient.png');
const GradientWhiteImg = require('../../assets/images/bg-gradient-white.png');

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

type HomeHeaderProps = {
  title?: string;
  subjectFilter: string | null;
  setSubjectFilter: (subject: string | null) => void;
  scrollY: any;
};

const HomeHeader = (props: HomeHeaderProps): JSX.Element => {
  const {title, subjectFilter, setSubjectFilter, scrollY} = props;

  const [subjectList, setSubjectList] = useState<string[]>(SUBJECTS);
  const [showGradient, setShowGradient] = useState(false);

  const {width: windowWidth} = useWindowDimensions();
  const isDarkMode = useColorScheme() === 'dark';

  const titleAnimatedStyles = useAnimatedStyle(() => {
    return {
      opacity: 1 - scrollY.value / 50,
      height: 50 - scrollY.value <= 0 ? 0 : 50 - scrollY.value,
      maxHeight: 50,
      transform: [
        {
          scale: 1 - scrollY.value / 150 > 1 ? 1 : 1 - scrollY.value / 150,
        },
      ],
    };
  });

  const scrollYAnimatedStyles = useAnimatedStyle(() => {
    return {
      opacity: scrollY.value / 50,
    };
  });

  const activSubjectBtnAnimatedStyles = useAnimatedStyle(() => {
    const currValue = windowWidth * 0.075 - 16 * (scrollY.value / 50);
    return {
      paddingHorizontal: currValue <= 16 ? 16 : currValue,
    };
  });

  const scrollViewAnimatedStyles = useAnimatedStyle(() => {
    const currValue = windowWidth * 0.075 - 16 * (scrollY.value / 50);
    if (subjectFilter) {
      return {
        paddingHorizontal: 0,
      };
    } else {
      return {
        paddingHorizontal:
          currValue <= 16
            ? 16
            : currValue > windowWidth * 0.075
            ? windowWidth * 0.075
            : currValue,
      };
    }
  });

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.absolute, scrollYAnimatedStyles]}>
        <BlurView
          reducedTransparencyFallbackColor={Theme.Colors.dark}
          blurType={isDarkMode ? 'dark' : 'light'}
          overlayColor={
            isDarkMode ? 'rgba(0,0,0,0.5)' : 'rgba(255,255,255,0.5)'
          }
          blurAmount={32}
          style={styles.absolute}
        />
      </Animated.View>

      <SafeAreaView edges={['top']} />
      <Animated.View style={[styles.contentWrapper]}>
        <Animated.View style={[styles.content]}>
          <View style={{flexDirection: 'row'}}>
            <Animated.View style={[titleAnimatedStyles]}>
              <FFText style={styles.title}>
                {title || 'Harry Potter Collection'}
              </FFText>
            </Animated.View>
          </View>

          <View style={{flex: 1, flexDirection: 'row'}}>
            {subjectFilter && (
              <Animated.View
                entering={FadeInRight}
                style={[
                  styles.activeFilterWrapper,
                  activSubjectBtnAnimatedStyles,
                ]}>
                <TouchableScale
                  key={subjectFilter}
                  duration={100}
                  haptic
                  style={[
                    styles.catButtonActive,
                    {
                      borderColor: isDarkMode
                        ? Theme.Colors.white
                        : Theme.Colors.black,
                      backgroundColor: 'rgba(255,255,255,.4)',
                    },
                  ]}
                  onPress={() => {
                    LayoutAnimation.configureNext(
                      LayoutAnimation.Presets.easeInEaseOut,
                    );
                    setSubjectFilter(null);
                  }}>
                  <FFText style={styles.catName}>{subjectFilter}</FFText>
                </TouchableScale>

                {showGradient && (
                  <ImageBackground
                    source={isDarkMode ? GradientImg : GradientWhiteImg}
                    resizeMode="stretch"
                    style={styles.gradientBg}
                  />
                )}
              </Animated.View>
            )}

            <Animated.ScrollView
              horizontal
              showsHorizontalScrollIndicator={false}
              scrollEventThrottle={16}
              onScroll={e => {
                if (e.nativeEvent.contentOffset.x > 0) {
                  !showGradient && setShowGradient(true);
                } else {
                  showGradient && setShowGradient(false);
                }
              }}
              style={[styles.catWrapper]}>
              <Animated.View
                style={[{flexDirection: 'row'}, scrollViewAnimatedStyles]}>
                {subjectList.map((item, index) => {
                  return (
                    <Animated.View
                      key={item + index}
                      style={{
                        position:
                          subjectFilter === item ? 'absolute' : 'relative',
                        left: subjectFilter === item ? '-50%' : '0%',
                        zIndex: subjectFilter === item ? 0 : 1,
                      }}>
                      <TouchableScale
                        key={item}
                        duration={100}
                        hapticOnPress
                        style={[
                          styles.catButton,
                          {
                            borderColor: isDarkMode
                              ? Theme.Colors.white
                              : Theme.Colors.black,
                          },
                        ]}
                        onPress={() => {
                          LayoutAnimation.configureNext(
                            LayoutAnimation.Presets.easeInEaseOut,
                          );
                          setSubjectFilter(item);
                        }}>
                        <FFText style={styles.catName}>{item}</FFText>
                      </TouchableScale>
                    </Animated.View>
                  );
                })}
              </Animated.View>
            </Animated.ScrollView>
          </View>
        </Animated.View>
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  absolute: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    backgroundColor: 'transparent',
    overflow: 'hidden',
    zIndex: 99,
  },
  contentWrapper: {
    zIndex: 99,
  },
  content: {
    position: 'relative',
    marginVertical: 16,
  },
  title: {
    paddingLeft: '7.5%',
    fontSize: 26,
    fontWeight: '500',
  },
  catWrapper: {
    flexDirection: 'row',
  },
  catButton: {
    marginRight: 16,
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderWidth: 1,
    borderRadius: 30,
    opacity: 1,
  },
  catName: {
    fontSize: 14,
    fontWeight: '700',
  },

  activeFilterWrapper: {
    paddingRight: 16,
    zIndex: 99,
  },
  catButtonActive: {
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderWidth: 1,
    borderRadius: 30,
    opacity: 1,
    zIndex: 999,
  },

  gradientBg: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    width: 56,
    borderBottomRightRadius: 14,
    borderBottomLeftRadius: 14,
    transform: [{rotate: '90deg'}, {translateY: -45}],
    zIndex: 1,
    opacity: 0.4,
    overflow: 'hidden',
  },
});

export default HomeHeader;
