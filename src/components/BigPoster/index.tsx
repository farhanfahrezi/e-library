import React from 'react';
import {ImageBackground, StyleSheet, useColorScheme} from 'react-native';

type BigPosterProps = {
  imageUri: string;
  onPress: () => void;
};

const BigPoster = (props: BigPosterProps): JSX.Element => {
  const {imageUri, onPress} = props;

  const isDarkMode = useColorScheme() === 'dark';

  return (
    <ImageBackground
      source={{
        uri: imageUri,
      }}
      style={[
        styles.container,
        {
          backgroundColor: isDarkMode
            ? 'rgba(0,0,0,0.5)'
            : 'rgba(255,255,255,0.5)',
        },
      ]}></ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 20,
    overflow: 'hidden',
  },
  posterWrapper: {
    flex: 1,
  },
  posterInfo: {
    padding: 20,
  },
});

export default BigPoster;
