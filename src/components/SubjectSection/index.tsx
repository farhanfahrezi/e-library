import React, {useEffect} from 'react';
import {
  ImageBackground,
  StyleSheet,
  useColorScheme,
  useWindowDimensions,
  View,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {SharedElement} from 'react-navigation-shared-element';
import {useGetBySubjectQuery} from '../../services/bookApi';
import Theme from '../../utils/Theme';
import FFText from '../FFText';
import TouchableScale from '../TouchableScale';

type BigPosterProps = {
  navigation: any;
  subject: string;
  onLayout: any;
};

const SubjectSection = (props: BigPosterProps): JSX.Element => {
  const {navigation, subject, onLayout} = props;

  const {width: windowWidth} = useWindowDimensions();
  const isDarkMode = useColorScheme() === 'dark';
  const {data, error, isLoading, isFetching, refetch} =
    useGetBySubjectQuery(subject);

  useEffect(() => {
    refetch();
  }, []);

  let _data = data?.works?.length > 0 ? data.works : [1, 2, 3, 4, 5];

  return (
    <View
      style={{
        backgroundColor: isDarkMode ? Theme.Colors.darker : Theme.Colors.white,
      }}
      onLayout={onLayout}>
      <FFText style={styles.subjectTitle}>{subject}</FFText>
      <ScrollView
        horizontal
        style={[styles.container]}
        contentContainerStyle={[
          styles.contentContainer,
          {paddingRight: windowWidth * 0.075 - 16},
        ]}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}>
        {_data.map((item: any, index: number) => (
          <TouchableScale
            hapticOnPress
            key={subject + item?.title + index || subject + item?.name + index}
            onPress={() => {
              navigation.navigate(
                'BookDetail',
                {book: item},
                {animationEnabled: false},
              );
            }}>
            <View style={{width: windowWidth * 0.34 + 16}}>
              <SharedElement id={`item.${item.cover_i || item.cover_id}.photo`}>
                <ImageBackground
                  source={{
                    uri: data
                      ? `https://covers.openlibrary.org/b/id/${
                          item.cover_id || item.cover_i
                        }-M.jpg`
                      : '',
                  }}
                  style={[
                    styles.bookImg,
                    {
                      width: windowWidth * 0.34,
                      height: windowWidth * 0.34 * 1.5,
                      backgroundColor: isDarkMode
                        ? 'rgba(0,0,0,.2)'
                        : 'rgba(255,255,255,0.2)',
                    },
                  ]}
                />
              </SharedElement>
              <FFText numberOfLines={2} style={styles.title}>
                {item.title || item.name}
              </FFText>
            </View>
          </TouchableScale>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: 30,
    zIndex: 9,
  },
  subjectTitle: {
    marginLeft: 16,
    marginBottom: 14,
    fontSize: 22,
    fontWeight: 'bold',
  },
  contentContainer: {
    paddingLeft: 16,
  },
  bookImg: {
    marginRight: 16,
    borderRadius: 10,
    overflow: 'hidden',
  },
  title: {
    paddingRight: 16,
    width: '100%',
    marginTop: 8,
    fontSize: 14,
    lineHeight: 22,
  },
});

export default SubjectSection;
