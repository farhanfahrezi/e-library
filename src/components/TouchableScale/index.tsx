import React, {useState} from 'react';
import {Animated, Pressable, StyleSheet} from 'react-native';
import {BaseButtonProps} from 'react-native-gesture-handler';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

interface TouchableScaleProps extends BaseButtonProps {
  style?: any;
  containerStyle?: any;
  children: JSX.Element;
  scaleTo?: number;
  duration?: number;
  haptic?: boolean;
  hapticOnPress?: boolean;
  onPress?: () => void;
}

const TouchableScale = (props: TouchableScaleProps): JSX.Element => {
  const {
    style,
    containerStyle,
    children,
    scaleTo = 0.96,
    duration = 200,
    onPress,
    haptic,
    hapticOnPress,
    ...rest
  } = props;

  const [scaleValue] = useState(new Animated.Value(1));

  const handlePressIn = () => {
    Animated.timing(scaleValue, {
      toValue: scaleTo,
      duration: duration,
      useNativeDriver: true,
    }).start();
  };

  const handlePressOut = () => {
    Animated.timing(scaleValue, {
      toValue: 1,
      duration: duration,
      useNativeDriver: true,
    }).start();
  };

  const triggerHaptic = () => {
    ReactNativeHapticFeedback.trigger('impactLight', options);
  };

  const animatedStyle = {
    transform: [{scale: scaleValue}],
  };

  return (
    <Animated.View style={[{flex: 1}, styles.container, animatedStyle]}>
      <Pressable
        onPressIn={() => {
          handlePressIn();
          haptic && triggerHaptic();
        }}
        // onTouchCancel={handlePressOut}
        onPressOut={() => {
          handlePressOut();
          haptic && triggerHaptic();
        }}
        onPress={() => {
          onPress && onPress();
          hapticOnPress && triggerHaptic();
          // hapticOnPress && setTimeout(() => triggerHaptic(), 100);
        }}
        style={[styles.container, style]}
        {...rest}>
        {children}
      </Pressable>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default TouchableScale;
