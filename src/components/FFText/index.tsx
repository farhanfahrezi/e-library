import React from 'react';
import {Text as RNText, StyleSheet, useColorScheme} from 'react-native';
import Theme from '../../utils/Theme';

interface RNTextProp extends React.ComponentProps<typeof RNText> {
  children: React.ReactNode;
  style?: any;
  onPressIn?: () => void;
}

export default function FFText({
  children,
  style,
  onPressIn,
  ...rest
}: RNTextProp) {
  const isDarkMode = useColorScheme() === 'dark';

  return (
    <RNText
      style={[
        {
          color: isDarkMode ? Theme.Colors.white : Theme.Colors.black,
        },
        styles.text,
        style,
      ]}
      {...rest}>
      {children}
    </RNText>
  );
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'SF Pro',
    backgroundColor: 'transparent',
  },
});
