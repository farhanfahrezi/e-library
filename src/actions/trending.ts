import API from '../utils/Api';
import {
  GET_TRENDING,
  GET_TRENDING_SUCCESS,
  GET_TRENDING_FAILURE,
} from './types';

export const getTrending = (params = {}) => {
  return (dispatch: any) => {
    dispatch({
      type: GET_TRENDING,
    });

    return API.get('/trending/all/week', params)
      .then((response: any) => {
        dispatch({
          type: GET_TRENDING_SUCCESS,
          payload: response.data,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: GET_TRENDING_FAILURE,
          payload: error,
        });
      });
  };
};
