import {BlurView} from '@react-native-community/blur';
import React, {useState} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  useColorScheme,
  useWindowDimensions,
  View,
} from 'react-native';
import {SharedElement} from 'react-navigation-shared-element';

import FFText from '../../components/FFText';
import TouchableScale from '../../components/TouchableScale';
import Theme from '../../utils/Theme';

const SuccessImg = require('../../assets/images/success.png');

const SuccessScreen = ({route, navigation}: any) => {
  const {book} = route.params;

  const isDarkMode = useColorScheme() === 'dark';
  const {width} = useWindowDimensions();

  const backgroundStyle = {
    flex: 1,
    backgroundColor: isDarkMode ? Theme.Colors.darker : Theme.Colors.white,
    zIndex: 99,
  };

  return (
    <View style={backgroundStyle}>
      <View style={styles.backgroundImg}>
        <ImageBackground
          style={styles.backgroundImg}
          source={{
            uri: `https://covers.openlibrary.org/b/id/${
              book?.cover_id || book?.cover_i
            }-M.jpg`,
          }}
        />
        <BlurView
          style={styles.blurBg}
          blurType={isDarkMode ? 'dark' : 'xlight'}
          blurAmount={32}
        />
      </View>

      <ScrollView
        horizontal={false}
        showsVerticalScrollIndicator={false}
        removeClippedSubviews
        overScrollMode="always"
        style={{flex: 1, zIndex: 99}}
        contentContainerStyle={{paddingHorizontal: 16, paddingBottom: 80}}>
        {/* <Image
          source={SuccessImg}
          style={styles.successImg}
          resizeMode="contain"
        /> */}

        <SharedElement
          id={`item.${book.cover_i || book.cover_id}.photo`}
          style={[
            styles.successImg,
            {
              alignSelf: 'center',
              width: width * 0.3,
              height: width * 0.45,
              borderRadius: 10,
              overflow: 'hidden',
            },
          ]}>
          <ImageBackground
            style={[
              {
                flex: 1,
                backgroundColor: isDarkMode
                  ? 'rgba(0,0,0,0.5)'
                  : 'rgba(255,255,255,0.5)',
              },
            ]}
            source={{
              uri: `https://covers.openlibrary.org/b/id/${
                book?.cover_id || book?.cover_i
              }-M.jpg`,
            }}
          />
        </SharedElement>

        <FFText style={styles.sectionTitle}>Congratulations!</FFText>
        <FFText style={styles.sectionItem}>
          You have successfully submitted the borrow book form for{' '}
          {book?.title || book?.name}. You are one step closer to enjoying this
          amazing book that will take you on a thrilling adventure. Whether you
          are looking for romance, mystery, fantasy, horror, or other, we hope
          you have a great time reading {book?.title || book?.name} and thank
          you for choosing our library!
        </FFText>

        <TouchableScale
          hapticOnPress
          onPress={() => navigation.popToTop()}
          style={[
            styles.borrowBtn,
            {
              borderColor: isDarkMode ? Theme.Colors.white : Theme.Colors.black,

              backgroundColor: isDarkMode
                ? 'rgba(0,0,0,0.2)'
                : 'rgba(255,255,255,0.2)',
            },
          ]}>
          <FFText style={styles.borrowBtnText}>Back to Home</FFText>
        </TouchableScale>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  blurBg: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    zIndex: 9,
  },
  backgroundImg: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 8,
  },
  successImg: {
    width: '80%',
    height: 200,
    alignSelf: 'center',
    marginTop: 80,
  },
  sectionTitle: {
    marginTop: 50,
    marginBottom: 14,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  sectionItem: {
    marginBottom: 30,
    fontSize: 16,
    textAlign: 'center',
    lineHeight: 26,
  },

  borrowBtn: {
    marginTop: 20,
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 16,
    borderRadius: 10,
    borderWidth: 0,
    overflow: 'hidden',
    zIndex: 99,
  },
  borrowBtnText: {
    marginTop: -2,
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default SuccessScreen;
