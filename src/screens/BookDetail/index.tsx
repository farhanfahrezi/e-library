import {BlurView} from '@react-native-community/blur';
import React from 'react';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  useColorScheme,
  useWindowDimensions,
  View,
} from 'react-native';
import {SharedElement} from 'react-navigation-shared-element';

import DetailPageHeader from '../../components/DetailPageHeader';
import FFText from '../../components/FFText';
import TouchableScale from '../../components/TouchableScale';
import {useGetByKeyQuery} from '../../services/bookApi';
import Theme from '../../utils/Theme';

const GradientImg = require('../../assets/images/bg-gradient.png');
const GradientWhiteImg = require('../../assets/images/bg-gradient-white.png');

const BookDetailScreen = ({route, navigation}: any) => {
  const {book} = route.params;
  const {data, currentData, error, isLoading, isFetching, refetch} =
    useGetByKeyQuery(book.key);

  const isDarkMode = useColorScheme() === 'dark';
  const {width} = useWindowDimensions();

  const backgroundStyle = {
    flex: 1,
    backgroundColor: isDarkMode ? Theme.Colors.darker : Theme.Colors.white,
    zIndex: 99,
  };

  return (
    <View style={backgroundStyle}>
      <View style={styles.backgroundImg}>
        <ImageBackground
          style={styles.backgroundImg}
          source={{
            uri: `https://covers.openlibrary.org/b/id/${
              book?.cover_id || book?.cover_i
            }-M.jpg`,
          }}
        />
        <BlurView
          style={styles.blurBg}
          blurType={isDarkMode ? 'dark' : 'xlight'}
          blurAmount={32}
        />
      </View>

      <ScrollView
        horizontal={false}
        showsVerticalScrollIndicator={false}
        removeClippedSubviews
        overScrollMode="always"
        style={{flex: 1, zIndex: 99}}
        contentContainerStyle={{paddingHorizontal: 16, paddingBottom: 80}}>
        <View
          style={{
            shadowColor: isDarkMode ? Theme.Colors.white : Theme.Colors.black,
            alignSelf: 'center',
            marginTop: 100,
            shadowOffset: {
              width: 0,
              height: 10,
            },
            shadowOpacity: 0.5,
            shadowRadius: 20,
            borderRadius: 16,
          }}>
          <View
            style={{
              width: width * 0.85,
              height: width * 1.2,
              borderRadius: 10,
              overflow: 'hidden',
            }}>
            <SharedElement
              id={`item.${book?.cover_id || book?.cover_i}.photo`}
              style={styles.bookCover}>
              <ImageBackground
                style={[
                  styles.bookCover,
                  {
                    backgroundColor: isDarkMode
                      ? 'rgba(0,0,0,0.5)'
                      : 'rgba(255,255,255,0.5)',
                  },
                ]}
                source={{
                  uri: `https://covers.openlibrary.org/b/id/${
                    book?.cover_id || book?.cover_i
                  }-L.jpg`,
                }}
              />
            </SharedElement>
          </View>
        </View>

        <View style={styles.infoWrapper}>
          <FFText style={styles.title} numberOfLines={3}>
            {book.title || book.name}
          </FFText>
          <FFText style={styles.author}>
            {book?.first_publish_year} •{' '}
            {book?.author_name && book?.author_name[0]}
            {book?.authors && book?.authors[0]?.name}
          </FFText>
        </View>

        <>
          <FFText style={styles.sectionTitle}>Summary</FFText>
          <FFText style={styles.sectionItem}>
            {data?.description?.value || 'No description'}
          </FFText>
        </>

        <>
          <FFText style={styles.sectionTitle}>Subject</FFText>
          <FFText style={styles.sectionItem}>
            {data?.subjects ? data?.subjects.join(' • ') : '-'}
          </FFText>
        </>
      </ScrollView>

      <View style={styles.bottomButtonWrapper}>
        <TouchableScale
          hapticOnPress
          onPress={() => navigation.navigate('Borrow', {book})}
          style={[
            styles.borrowBtn,
            {
              borderColor: isDarkMode ? Theme.Colors.white : Theme.Colors.black,
            },
          ]}>
          <FFText style={styles.borrowBtnText}>Borrow</FFText>
        </TouchableScale>
      </View>

      <DetailPageHeader navigation={navigation} title="Book Detail" />
    </View>
  );
};

const styles = StyleSheet.create({
  blurBg: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    // height: 800,
    zIndex: 9,
  },
  backgroundImg: {
    ...StyleSheet.absoluteFillObject,
    // height: 800,
    zIndex: 8,
  },
  bookCover: {
    flex: 1,
    borderRadius: 16,
    // overflow: 'hidden',
  },

  infoWrapper: {
    paddingTop: 30,
    paddingHorizontal: 16,
    marginBottom: 50,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '700',
    letterSpacing: 0.5,
  },
  author: {
    marginTop: 6,
    textAlign: 'center',
    fontSize: 18,
    opacity: 0.7,
  },
  sectionTitle: {
    marginBottom: 14,
    fontSize: 20,
    fontWeight: 'bold',
  },
  sectionItem: {
    marginBottom: 30,
    fontSize: 16,
    lineHeight: 26,
  },

  bottomButtonWrapper: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 30,
    zIndex: 999,
  },
  borrowBlurBg: {
    flex: 1,
    padding: 16,
  },
  borrowBtn: {
    paddingHorizontal: 20,
    paddingVertical: 8,
    borderRadius: 50,
    borderWidth: 2,
    overflow: 'hidden',
    backgroundColor: '#4396F7',
    zIndex: 99,
  },
  borrowBtnText: {
    marginTop: -2,
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default BookDetailScreen;
