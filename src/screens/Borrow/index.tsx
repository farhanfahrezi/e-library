import {BlurView} from '@react-native-community/blur';
import React, {useState} from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  ScrollView,
  StyleSheet,
  useColorScheme,
  useWindowDimensions,
  View,
} from 'react-native';
import DatePicker from 'react-native-date-picker';
import {SharedElement} from 'react-navigation-shared-element';

import DetailPageHeader from '../../components/DetailPageHeader';
import FFText from '../../components/FFText';
import TouchableScale from '../../components/TouchableScale';
import Theme from '../../utils/Theme';

const BorrowScreen = ({route, navigation}: any) => {
  const {book} = route.params;
  const [date, setDate] = useState(new Date());
  const [loading, setLoading] = useState(false);

  const isDarkMode = useColorScheme() === 'dark';
  const {width} = useWindowDimensions();

  const submit = () => {
    if (loading) return;
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      navigation.pop(1);
      navigation.replace('Success', {book});
    }, 3000);
  };

  const backgroundStyle = {
    flex: 1,
    backgroundColor: isDarkMode ? Theme.Colors.darker : Theme.Colors.white,
    zIndex: 99,
  };

  return (
    <View style={backgroundStyle}>
      <View style={styles.backgroundImg}>
        <ImageBackground
          style={styles.backgroundImg}
          source={{
            uri: `https://covers.openlibrary.org/b/id/${
              book?.cover_id || book?.cover_i
            }-M.jpg`,
          }}
        />
        <BlurView
          style={styles.blurBg}
          blurType={isDarkMode ? 'dark' : 'xlight'}
          blurAmount={32}
        />
      </View>

      <ScrollView
        horizontal={false}
        showsVerticalScrollIndicator={false}
        removeClippedSubviews
        overScrollMode="always"
        style={{flex: 1, zIndex: 99}}
        contentContainerStyle={{paddingHorizontal: 16, paddingBottom: 80}}>
        <View style={{flexDirection: 'row'}}>
          <View
            style={{
              shadowColor: isDarkMode ? Theme.Colors.white : Theme.Colors.black,
              alignSelf: 'center',
              marginTop: 80,
              shadowOffset: {
                width: 0,
                height: 10,
              },
              shadowOpacity: 0.5,
              shadowRadius: 20,
              borderRadius: 16,
            }}>
            <SharedElement
              id={`item.${book.cover_i || book.cover_id}.photo`}
              style={{
                width: width * 0.3,
                height: width * 0.45,
                borderRadius: 10,
                overflow: 'hidden',
              }}>
              <ImageBackground
                style={[
                  styles.bookCover,
                  {
                    backgroundColor: isDarkMode
                      ? 'rgba(0,0,0,0.5)'
                      : 'rgba(255,255,255,0.5)',
                  },
                ]}
                source={{
                  uri: `https://covers.openlibrary.org/b/id/${
                    book?.cover_id || book?.cover_i
                  }-M.jpg`,
                }}
              />
            </SharedElement>
          </View>

          <View style={styles.infoWrapper}>
            <FFText style={styles.title} numberOfLines={3}>
              {book.title || book.name}
            </FFText>
            <FFText style={styles.author}>
              {book?.first_publish_year} •{' '}
              {book?.author_name && book?.author_name[0]}
              {book?.authors && book?.authors[0]?.name}
            </FFText>
          </View>
        </View>

        <FFText style={styles.sectionItem}>
          You're almost done! Just select the date when you want to pick up your
          book from the library. You can choose any day within the next two
          weeks.
        </FFText>

        <FFText style={styles.sectionTitle}>Select Pickup Date</FFText>
        <View
          style={{
            paddingHorizontal: 16,
            borderRadius: 16,
            overflow: 'hidden',
            backgroundColor: isDarkMode
              ? 'rgba(0,0,0,0.2)'
              : 'rgba(255,255,255,0.2)',
          }}>
          <DatePicker
            theme={isDarkMode ? 'dark' : 'light'}
            style={{width: width - 64}}
            androidVariant="iosClone"
            fadeToColor="none"
            textColor={isDarkMode ? Theme.Colors.white : Theme.Colors.black}
            mode="date"
            minimumDate={new Date()}
            maximumDate={
              new Date(new Date().setDate(new Date().getDate() + 14))
            }
            date={date}
            onDateChange={setDate}
          />
        </View>
        <TouchableScale
          hapticOnPress
          onPress={submit}
          enabled={!loading}
          style={[
            styles.borrowBtn,
            {
              borderColor: isDarkMode ? Theme.Colors.white : Theme.Colors.black,

              backgroundColor: isDarkMode
                ? 'rgba(0,0,0,0.2)'
                : 'rgba(255,255,255,0.2)',
            },
          ]}>
          {loading ? (
            <ActivityIndicator size="small" color={Theme.Colors.white} />
          ) : (
            <FFText style={styles.borrowBtnText}>Confirm</FFText>
          )}
        </TouchableScale>
      </ScrollView>

      <DetailPageHeader navigation={navigation} title="Borrow Book" />
    </View>
  );
};

const styles = StyleSheet.create({
  blurBg: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    zIndex: 9,
  },
  backgroundImg: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 8,
  },
  bookCover: {
    flex: 1,
    borderRadius: 16,
  },

  infoWrapper: {
    flex: 1,
    marginTop: 80,
    paddingHorizontal: 16,
    marginBottom: 50,
  },
  title: {
    fontSize: 20,
    lineHeight: 30,
    fontWeight: '700',
    letterSpacing: 0.5,
  },
  author: {
    marginTop: 6,
    fontSize: 18,
    opacity: 0.7,
  },
  sectionTitle: {
    marginBottom: 14,
    fontSize: 20,
    fontWeight: 'bold',
  },
  sectionItem: {
    marginTop: 30,
    marginBottom: 30,
    fontSize: 16,
    lineHeight: 26,
  },
  borrowBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingHorizontal: 20,
    paddingVertical: 16,
    height: 50,
    borderRadius: 10,
    borderWidth: 0,
    overflow: 'hidden',
    zIndex: 99,
  },
  borrowBtnText: {
    marginTop: -2,
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default BorrowScreen;
