import React, {useEffect, useRef, useState} from 'react';
import {useColorScheme} from 'react-native';
import {GestureHandlerRootView, ScrollView} from 'react-native-gesture-handler';
import {useSharedValue} from 'react-native-reanimated';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import BigPosterSection from '../../components/BigPosterSection';
import HomeHeader from '../../components/HomeHeader';
import SubjectSection from '../../components/SubjectSection';
import {SUBJECTS} from '../../utils/Constant';
import Theme from '../../utils/Theme';

const HomeScreen = ({navigation}: any) => {
  const scrollY = useRef(useSharedValue(1)).current;
  const [subjectFilter, setSubjectFilter] = useState<string | null>(null);
  const [subjectSectionPos, setSubjectSectionPos] = useState<any>({});

  const isDarkMode = useColorScheme() === 'dark';
  const insets = useSafeAreaInsets();

  const _scrollView = useRef<ScrollView>(null);

  useEffect(() => {
    if (subjectFilter) {
      _scrollView.current?.scrollTo({
        x: 0,
        y: subjectSectionPos[subjectFilter] - 80 - insets.top,
        animated: true,
      });
    }
  }, [subjectFilter]);

  const backgroundStyle = {
    flex: 1,
    backgroundColor: isDarkMode ? Theme.Colors.darker : Theme.Colors.white,
  };

  const onScroll = (event: any) => {
    const offsetY = event.nativeEvent.contentOffset.y;
    scrollY.value = offsetY;
  };

  const onScrollEndDrag = (event: any) => {
    const offsetY = event.nativeEvent.contentOffset.y;
    if (offsetY < 50) {
      _scrollView.current?.scrollTo({x: 0, y: 0, animated: true});
    }
  };

  return (
    <GestureHandlerRootView style={backgroundStyle}>
      <ScrollView
        ref={_scrollView}
        horizontal={false}
        showsVerticalScrollIndicator={false}
        onScroll={onScroll}
        onScrollEndDrag={onScrollEndDrag}
        // onMomentumScrollEnd={onScrollEndDrag}
        scrollEventThrottle={16}
        needsOffscreenAlphaCompositing
        removeClippedSubviews
        overScrollMode="always"
        style={[backgroundStyle]}>
        <BigPosterSection navigation={navigation} />
        {SUBJECTS.map((subject, index) => (
          <SubjectSection
            onLayout={(event: any) => {
              if (event?.nativeEvent?.layout?.y) {
                setSubjectSectionPos({
                  ...subjectSectionPos,
                  [subject]: event?.nativeEvent?.layout?.y,
                });
              }
            }}
            key={subject + subject + index}
            navigation={navigation}
            subject={subject}
          />
        ))}
      </ScrollView>
      <HomeHeader
        scrollY={scrollY}
        subjectFilter={subjectFilter}
        setSubjectFilter={(item: string | null) => setSubjectFilter(item)}
      />
    </GestureHandlerRootView>
  );
};

export default HomeScreen;
