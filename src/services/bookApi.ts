// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
// import type { Pokemon } from './types'

// Define a service using a base URL and expected endpoints
export const bookApi = createApi({
  reducerPath: 'bookApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://openlibrary.org/' }),
  endpoints: (builder) => ({
    getHarryPotter: builder.query<any, string>({
      query: () => `search.json?q=title:harry+potter language:eng author:J. K. Rowling&fields=*,availability&offset=0&limit=50&sort=new`,
      transformResponse: (response: any) => {
        const { docs } = response;
        const booksWithCover = docs.filter((doc: any) => doc.cover_i || doc.cover_id).slice(0, 15)
        return {
          ...response,
          docs: booksWithCover
        }
      }
    }),
    getBySubject: builder.query<any, string>({
      query: (subject: string) => `subjects/${subject.replace(' ', '_').toLowerCase()}.json?offset=0&limit=30&sort=new`,
      transformResponse: (response: any) => {
        const { works } = response;
        const booksWithCover = works.filter((doc: any) => doc.cover_i || doc.cover_id).slice(0, 10)

        return {
          ...response,
          works: booksWithCover
        }
      },
    }),
    getByKey: builder.query<any, string>({
      query: (key: string) => `${key}.json`,
      // transformResponse: (response: any) => {
      //   const { works } = response;
      //   const booksWithCover = works.filter((doc: any) => doc.cover_i || doc.cover_id).slice(0, 10)

      //   return {
      //     ...response,
      //     works: booksWithCover
      //   }
      // },
    }),
  }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetHarryPotterQuery, useGetBySubjectQuery, useGetByKeyQuery } = bookApi