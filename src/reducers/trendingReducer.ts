import { GET_TRENDING, GET_TRENDING_SUCCESS, GET_TRENDING_FAILURE } from "../actions/types";

const initialState = {
    trending: {
        loading: false,
        page: 1,
        results: [],
        total_pages: 0,
        total_results: 0,
        error: null,
    }
};

const trendingReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case GET_TRENDING: {
            return {
                ...state,
                trending: {
                    ...state.trending,
                    loading: true,
                }
            };
        }
        case GET_TRENDING_SUCCESS: {
            return {
                ...state,
                trending: {
                    ...state.trending,
                    ...action.payload,
                    loading: false,
                }
            };
        }
        case GET_TRENDING_FAILURE: {
            return {
                ...state,
                trending: {
                    ...state.trending,
                    loading: false,
                    error: action.payload,
                }
            };
        }
        default:
            return state;
    }
}

export default trendingReducer;