/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import {NavigationContainer} from '@react-navigation/native';
import type {PropsWithChildren} from 'react';
import React from 'react';
import {useColorScheme} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux';
import store from './src/store/store';

import {CardStyleInterpolators} from '@react-navigation/stack';
import 'react-native-gesture-handler';
import {enableScreens} from 'react-native-screens';
import BookDetailScreen from './src/screens/BookDetail';
import BorrowScreen from './src/screens/Borrow';
import HomeScreen from './src/screens/Home';
import SuccessScreen from './src/screens/Success';
import Theme from './src/utils/Theme';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
import {forModalPresentationIOS} from './src/utils/forModalPresentationIOS';

enableScreens(true);

const Stack = createSharedElementStackNavigator();

type SectionProps = PropsWithChildren<{
  title: string;
}>;
function App(): JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Theme.Colors.darker : Theme.Colors.white,
  };

  return (
    <Provider store={store}>
      <SafeAreaProvider>
        <NavigationContainer
          theme={{
            dark: isDarkMode,
            colors: {
              primary: Theme.Colors.primary,
              background: isDarkMode ? Theme.Colors.darker : Theme.Colors.white,
              card: 'transparent',
              text: isDarkMode ? Theme.Colors.white : Theme.Colors.black,
              border: isDarkMode ? Theme.Colors.white : Theme.Colors.black,
              notification: Theme.Colors.primary,
            },
          }}>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
              presentation: 'modal',
              cardStyleInterpolator: forModalPresentationIOS,
              // orientation: 'portrait',
              // animation: 'fade',
              // freezeOnBlur: true,
              // contentStyle: {
              //   backgroundColor: isDarkMode
              //     ? Theme.Colors.darker
              //     : Theme.Colors.white,
              // },
            }}>
            <Stack.Screen
              name="Home"
              component={HomeScreen}
              sharedElements={(route, otherRoute, showing) => {
                const {book} = route.params;
                return [
                  {
                    id: `item.${book?.cover_id || book?.cover_i}.photo`,
                    // animation: 'fade',
                    resize: 'clip',
                  },
                ];
              }}
            />
            <Stack.Screen
              name="BookDetail"
              component={BookDetailScreen}
              sharedElements={(route, otherRoute, showing) => {
                const {book} = route.params;
                return [
                  {
                    id: `item.${book?.cover_id || book?.cover_i}.photo`,
                    // animation: 'fade',
                    resize: 'clip',
                  },
                ];
              }}
            />
            <Stack.Screen
              name="Borrow"
              component={BorrowScreen}
              sharedElements={(route, otherRoute, showing) => {
                const {book} = route.params;
                return [
                  {
                    id: `item.${book?.cover_id || book?.cover_i}.photo`,
                    // animation: 'fade',
                    resize: 'clip',
                  },
                ];
              }}
            />
            <Stack.Screen name="Success" component={SuccessScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    </Provider>
  );
}

export default App;
